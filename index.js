var r = require('ramda'),
	rf = require('ramda-fantasy');

for(var key in rf) {
	if(rf.hasOwnProperty(key)) {
		r[key] = rf[key];
	}
}

r.mixin = function(scope) {
	for(var key in r) {
		if(r.hasOwnProperty(key)) {
			if(key !== 'mixin') {
				scope[key] = r[key];
			}
		}
	}
};

module.exports = r;
